﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula消息中心控件
    /// <para>继承UserControl类，实现Gizeta.Macula.Controls.INotificationComponent接口</para>
    /// </summary>
    public partial class NotificationComponent : UserControl, INotificationComponent
    {
        private PlayerMediator mediator = PlayerMediator.Instance;

        public NotificationComponent()
        {
            InitializeComponent();
            mediator.NotificationPanel = this;
        }

        #region INotificationComponent接口实现

        public INotificationComponent AddMessage(string message)
        {
            if (MessagePanel.Visibility == Visibility.Collapsed)
            {
                MessagePanel.Visibility = Visibility.Visible;
            }
            TextBlock textBlock = new TextBlock();
            textBlock.Text = message;
            textBlock.Style = App.Current.Resources["NotificationComponentMessageStyle"] as Style;
            MessagePanel.Children.Add(textBlock);

            SplineDoubleKeyFrame frame1 = new SplineDoubleKeyFrame() { KeyTime = TimeSpan.Zero, Value = 1 };
            SplineDoubleKeyFrame frame2 = new SplineDoubleKeyFrame() { KeyTime = TimeSpan.FromSeconds(3), Value = 1 };
            SplineDoubleKeyFrame frame3 = new SplineDoubleKeyFrame() { KeyTime = TimeSpan.FromSeconds(4), Value = 0 };
            DoubleAnimationUsingKeyFrames fadeAnimation = new DoubleAnimationUsingKeyFrames() { KeyFrames = { frame1, frame2, frame3 } };
            Storyboard storyBoard = new Storyboard() { Children = { fadeAnimation } };
            Storyboard.SetTarget(fadeAnimation, textBlock);
            Storyboard.SetTargetProperty(fadeAnimation, new PropertyPath("Opacity"));
            storyBoard.Completed += storyBoard_Completed;
            storyBoard.Begin();

            return this;
        }

        public INotificationComponent HideMessage()
        {
            MessagePanel.Visibility = Visibility.Collapsed;
            return this;
        }

        public void Initialize()
        {
            HideMessage();
        }

        #endregion

        private void storyBoard_Completed(object sender, EventArgs e)
        {
            Storyboard sb = sender as Storyboard;
            sb.Completed -= storyBoard_Completed;
            sb = null;
            MessagePanel.Children.RemoveAt(0);
        }
    }
}

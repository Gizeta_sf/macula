﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// 视频播放器
    /// <para>继承UserControl类，Gizeta.Macula.Controls.IPlayerComponent接口</para>
    /// </summary>
    public partial class PlayerComponent : UserControl, IPlayerComponent
    {
        private PlayerMediator mediator = PlayerMediator.Instance;

        public PlayerComponent()
        {
            InitializeComponent();
            mediator.PlayerContainer = this;
            VideoPlayer.MediaOpened += VideoPlayer_MediaOpened;
        }

        #region IPlayerComponent接口实现

        public IPlayer Player
        {
            get { return VideoPlayer; }
        }

        public IPlayerComponent SetCover(Uri url)
        {
            CoverImage.Source = new BitmapImage(url);
            return this;
        }

        public void Initialize()
        {
            LayoutRoot.MouseLeftButtonUp -= LayoutRoot_MouseLeftButtonUp;
            LayoutRoot.KeyDown -= LayoutRoot_KeyDown;
            VideoPlayer.CurrentStateChanged -= VideoPlayer_CurrentStateChanged;
            VideoPlayer.BufferingProgressChanged -= VideoPlayer_BufferingProgressChanged;

            CoverImage.Source = null;
            CoverImage.Visibility = Visibility.Visible;
            PauseSymbolCanvas.Visibility = Visibility.Collapsed;
            BufferSymbolCanvas.Visibility = Visibility.Collapsed;
            this.Cursor = Cursors.Arrow;

            VideoPlayer.Initialize();
        }

        #endregion

        private void SetBufferStyle(double percent)
        {
            if (percent < 1)
            {
                if (BufferSymbolCanvas.Visibility == Visibility.Collapsed)
                {
                    BufferSymbolCanvas.Visibility = Visibility.Visible;
                    BufferAnimation.Begin();
                }
            }
            else
            {
                if (BufferSymbolCanvas.Visibility == Visibility.Visible)
                {
                    BufferAnimation.Stop();
                    BufferSymbolCanvas.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void ChangePlayerStatus()
        {
            switch (VideoPlayer.CurrentState)
            {
                case MediaElementState.Playing:
                case MediaElementState.Buffering:
                    VideoPlayer.Pause();
                    break;
                case MediaElementState.Paused:
                case MediaElementState.Stopped:
                    CoverImage.Visibility = Visibility.Collapsed;
                    VideoPlayer.Play();
                    break;
                default: break;
            }
        }

        #region 事件处理

        private void LayoutRoot_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (PlayerConfig.Instance.AllowDefaultMouseControl)
            {
                ChangePlayerStatus();
                Focus();
            }
        }

        private void VideoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (PlayerConfig.Instance.AllowDefaultMouseControl)
            {
                this.Cursor = Cursors.Hand;
            }
            LayoutRoot.MouseLeftButtonUp += LayoutRoot_MouseLeftButtonUp;
            LayoutRoot.KeyDown += LayoutRoot_KeyDown;
            VideoPlayer.BufferingProgressChanged += VideoPlayer_BufferingProgressChanged;
            VideoPlayer.CurrentStateChanged += VideoPlayer_CurrentStateChanged;

            PauseSymbolCanvas.Visibility = Visibility.Visible;
            BufferSymbolCanvas.Visibility = Visibility.Collapsed;
            BufferAnimation.Stop();

            this.Focus();
        }

        private void VideoPlayer_BufferingProgressChanged(object sender, RoutedEventArgs e)
        {
            SetBufferStyle(VideoPlayer.BufferingProgress);
        }

        private void VideoPlayer_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (VideoPlayer.CurrentState)
            {
                case MediaElementState.Playing:
                case MediaElementState.Buffering:
                    PauseSymbolCanvas.Visibility = Visibility.Collapsed;
                    break;
                case MediaElementState.Paused:
                case MediaElementState.Stopped:
                    PauseSymbolCanvas.Visibility = Visibility.Visible;
                    break;
                default: break;
            }
        }

        private void LayoutRoot_KeyDown(object sender, KeyEventArgs e)
        {
            if (PlayerConfig.Instance.AllowDefaultKeyboardControl)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    switch (e.Key)
                    {
                        case Key.F:
                            PlayerConfig.Instance.IsFullScreen = !PlayerConfig.Instance.IsFullScreen;
                            break;
                        default: break;
                    }
                }
                else
                {
                    switch (e.Key)
                    {
                        case Key.Space:
                            ChangePlayerStatus();
                            break;
                        case Key.Escape:
                            break;
                        case Key.Left:
                            if (VideoPlayer.CanSeek)
                            {
                                if (VideoPlayer.Position > TimeSpan.FromSeconds(5))
                                {
                                    VideoPlayer.Position -= TimeSpan.FromSeconds(5);
                                }
                                else
                                {
                                    VideoPlayer.Position = TimeSpan.Zero;
                                }
                            }
                            break;
                        case Key.Right:
                            if (VideoPlayer.CanSeek)
                            {
                                if (VideoPlayer.Position < Player.NaturalDuration.TimeSpan - TimeSpan.FromSeconds(5))
                                {
                                    VideoPlayer.Position += TimeSpan.FromSeconds(5);
                                }
                                else
                                {
                                    VideoPlayer.Position = Player.NaturalDuration.TimeSpan;
                                }
                            }
                            break;
                        case Key.Up:
                            PlayerConfig.Instance.Volume += 0.05;
                            break;
                        case Key.Down:
                            PlayerConfig.Instance.Volume -= 0.05;
                            break;
                        default: break;
                    }
                }
            }
        }

        #endregion
    }
}

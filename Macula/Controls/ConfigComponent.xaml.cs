﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using Gizeta.Macula.Plugin;
using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula设置页
    /// <para>继承UserControl类，实现Gizeta.Macula.Controls.IConfigComponent接口</para>
    /// </summary>
    public partial class ConfigComponent : UserControl, IConfigComponent
    {
        private PlayerMediator mediator = PlayerMediator.Instance;

        public ConfigComponent()
        {
            InitializeComponent();
            mediator.ConfigPage = this;
            this.DataContext = PlayerConfig.Instance;

            if (!Application.Current.IsRunningOutOfBrowser) return;
            ConfigTabControl.SelectedIndex = 0;
            FilePage.Visibility = Visibility.Visible;
            PluginPage.Visibility = Visibility.Visible;
            FileChooseButton.Click += FileChooseButton_Click;
            PlayParamsButton.Click += PlayParamsButton_Click;
            ClearParamsButton.Click += ClearParamsButton_Click;
        }

        #region IConfigComponent接口实现

        public void Initialize()
        {
        }

        public void BindPluginList()
        {
            VideoParserDataGrid.ItemsSource = from vpi in PluginInfoManager.Instance.VideoParserInfoList
                                              where vpi.TargetType != "http" && vpi.TargetType != "local"
                                              select vpi;
            PlayerPluginDataGrid.ItemsSource = PluginInfoManager.Instance.PlayerPluginInfoList;

            if (!Application.Current.IsRunningOutOfBrowser) return;
            VideoTypeComboBox.ItemsSource = PluginInfoManager.Instance.VideoParserInfoList;
            VideoTypeComboBox.DisplayMemberPath = "TargetType";
            VideoTypeComboBox.SelectedValuePath = "TargetType";
            VideoTypeComboBox.SelectedIndex = 0;
            VideoTypeComboBox.SelectionChanged += VideoTypeComboBox_SelectionChanged;
        }

        #endregion

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            LayoutRoot.Width = this.ActualWidth;
            LayoutRoot.Height = this.ActualHeight;
        }

        private void FileChooseButton_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog { Filter = "视频文件(*.mp4)|*.mp4|所有文件|*.*", Multiselect = false };
            if (fileDialog.ShowDialog() == true)
            {
                VideoFileTextBox.Text = fileDialog.File.FullName;
            }
        }

        private void ClearParamsButton_Click(object sender, RoutedEventArgs e)
        {
            VideoFileTextBox.Text = "";
        }

        private void PlayParamsButton_Click(object sender, RoutedEventArgs e)
        {
            mediator.PlayVideo(new Dictionary<string, string>() { { "vtype", VideoTypeComboBox.SelectedValue as string }, { "vid", VideoFileTextBox.Text } });
        }

        private void VideoTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FileChooseButton.IsEnabled = VideoTypeComboBox.SelectedValue as string == "local";
        }
    }
}

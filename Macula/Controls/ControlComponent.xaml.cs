﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula视频控制条
    /// <para>继承UserControl类，实现Gizeta.Macula.Controls.IControlComponent接口</para>
    /// </summary>
    public partial class ControlComponent : UserControl, IControlComponent
    {
        private DispatcherTimer VideoTimer = new DispatcherTimer(); // 视频时间计时器

        private PlayerMediator mediator = PlayerMediator.Instance;
        private IPlayer player;

        public ControlComponent()
        {
            InitializeComponent();
            mediator.ControlBar = this;
            this.DataContext = PlayerConfig.Instance;

            VolumeButton.MouseEnter += VolumeButton_MouseEnter;
            FullScreenButton.MouseEnter += FullScreenButton_MouseEnter;
        }

        #region IControlBar接口实现

        public void Initialize()
        {
            PlayButton.IsEnabled = false;
            TimeSlider.IsEnabled = false;
            CurrentTimeTextBlock.Text = "0:00";
            TotalTimeTextBlock.Text = "";
            CurrentTimeTextBlock.Visibility = Visibility.Visible;
            CurrentTimeTextBox.Visibility = Visibility.Collapsed;
            PlayButton.IsChecked = false;
            TimeSlider.Value = 0;

            VideoTimer.Stop();
            VideoTimer.Tick -= VideoTimer_Tick;
            PlayButton.Click -= PlayButton_Click;
            LoopButton.Click -= Button_Click;
            FullScreenButton.Click -= Button_Click;
            VolumeButton.Click -= Button_Click;
            WideScreenButton.Click -= Button_Click;
            CurrentTimeTextBlock.MouseLeftButtonUp -= CurrentTimeTextBlock_MouseLeftButtonUp;
            CurrentTimeTextBox.KeyDown -= CurrentTimeTextBox_KeyDown;
            TimeSlider.MyValueChanged -= TimeSlider_ValueChanged;
            TimeSlider.MyValueChangedInDrag -= TimeSlider_ValueChanged;
            player = mediator.PlayerContainer.Player;
            player.MediaOpened += player_MediaOpened;
            player.CurrentStateChanged -= player_CurrentStateChanged;
            player.MediaEnded -= player_MediaEnded;
        }

        #endregion

        #region 事件处理

        private void player_MediaOpened(object sender, EventArgs e)
        {
            PlayButton.Click += PlayButton_Click;
            PlayButton.IsEnabled = true;
            if (player.CanSeek)
            {
                CurrentTimeTextBlock.MouseLeftButtonUp += CurrentTimeTextBlock_MouseLeftButtonUp;
                TimeSlider.IsEnabled = true;
                TimeSlider.MyValueChanged += TimeSlider_ValueChanged;
                TimeSlider.MyValueChangedInDrag += TimeSlider_ValueChanged;
            }
            TimeSpan totalTime = player.NaturalDuration.HasTimeSpan ? player.NaturalDuration.TimeSpan : TimeSpan.Zero;
            TimeSlider.Maximum = totalTime.TotalSeconds;
            if (totalTime > TimeSpan.Zero)
            {
                TotalTimeTextBlock.Text = string.Format("/{0}:{1}", (totalTime.Hours * 60 + totalTime.Minutes).ToString(), totalTime.Seconds.ToString().PadLeft(2, '0'));
            }
            else
            {
                TotalTimeTextBlock.Text = "";
            }
            VideoTimer.Interval = TimeSpan.FromMilliseconds(50);
            VideoTimer.Tick += VideoTimer_Tick;
            LoopButton.Click += Button_Click;
            FullScreenButton.Click += Button_Click;
            VolumeButton.Click += Button_Click;
            WideScreenButton.Click += Button_Click;
            player.CurrentStateChanged += player_CurrentStateChanged;
            player.MediaEnded += player_MediaEnded;

            VideoTimer.Start();
        }

        private void player_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeSlider.MyValueChanged -= TimeSlider_ValueChanged;
            TimeSlider.Value = 0;
            CurrentTimeTextBlock.Text = "0:00";
            TimeSlider.MyValueChanged += TimeSlider_ValueChanged;
        }

        private void CurrentTimeTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CurrentTimeTextBox.Visibility = Visibility.Visible;
            CurrentTimeTextBlock.Visibility = Visibility.Collapsed;
            CurrentTimeTextBox.Text = CurrentTimeTextBlock.Text;
            CurrentTimeTextBox.Focus();
            CurrentTimeTextBox.KeyDown += CurrentTimeTextBox_KeyDown;
            CurrentTimeTextBox.LostFocus += CurrentTimeTextBox_LostFocus;
        }

        private void CurrentTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            CurrentTimeTextBlock.Visibility = Visibility.Visible;
            CurrentTimeTextBox.Visibility = Visibility.Collapsed;
            CurrentTimeTextBox.KeyDown -= CurrentTimeTextBox_KeyDown;
            CurrentTimeTextBox.LostFocus -= CurrentTimeTextBox_LostFocus;

            mediator.PlayerContainer.Focus();
        }

        private void CurrentTimeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int t1,t2;
                if (CurrentTimeTextBox.Text.Contains(":"))
                {
                    string[] temp = CurrentTimeTextBox.Text.Split(':');
                    int.TryParse(temp[0], out t1);
                    int.TryParse(temp[1], out t2);
                    player.Position = TimeSpan.FromSeconds(t1 * 60 + t2);
                }
                else
                {
                    int.TryParse(CurrentTimeTextBox.Text, out t1);
                    player.Position = TimeSpan.FromSeconds(t1);
                }
                CurrentTimeTextBlock.Visibility = Visibility.Visible;
                CurrentTimeTextBox.Visibility = Visibility.Collapsed;
                CurrentTimeTextBox.KeyDown -= CurrentTimeTextBox_KeyDown;
                CurrentTimeTextBox.LostFocus -= CurrentTimeTextBox_LostFocus;

                mediator.PlayerContainer.Focus();
            }
            else if (e.Key == Key.Escape)
            {
                CurrentTimeTextBlock.Visibility = Visibility.Visible;
                CurrentTimeTextBox.Visibility = Visibility.Collapsed;
                CurrentTimeTextBox.KeyDown -= CurrentTimeTextBox_KeyDown;
                CurrentTimeTextBox.LostFocus -= CurrentTimeTextBox_LostFocus;

                mediator.PlayerContainer.Focus();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mediator.PlayerContainer.Focus();
        }

        private void player_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (player.CurrentState)
            {
                case MediaElementState.Playing:
                case MediaElementState.Buffering:
                    PlayButton.IsChecked = true;
                    PlayButton.Content = "Pause";
                    break;
                default:
                    PlayButton.IsChecked = false;
                    PlayButton.Content = "Play";
                    break;
            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (PlayButton.IsChecked.Value)
            {
                player.Play();
            }
            else
            {
                player.Pause();
            }
            mediator.PlayerContainer.Focus();
        }

        private void VideoTimer_Tick(object sender, EventArgs e)
        {
            if (player.Position.TotalSeconds != TimeSlider.Value)
            {
                TimeSpan position = player.Position;
                CurrentTimeTextBlock.Text = string.Format("{0}:{1}", (position.Hours * 60 + position.Minutes).ToString(), position.Seconds.ToString().PadLeft(2, '0'));
                if (!TimeSlider.IsInDrag)
                {
                    TimeSlider.MyValueChanged -= TimeSlider_ValueChanged;
                    TimeSlider.Value = position.TotalSeconds;
                    TimeSlider.MyValueChanged += TimeSlider_ValueChanged;
                }
            }
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            player.Position = TimeSpan.FromSeconds(TimeSlider.Value);
        }

        private void VolumeButton_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Height = double.NaN;
            VolumeSlider.Visibility = Visibility.Visible;
            VolumePanel.Margin = new Thickness(0, -80, 0, 0);
            VolumePanel.Background = new SolidColorBrush(Color.FromArgb(255, 68, 68, 68));
            VolumePanel.MouseLeave += VolumeStackPanel_MouseLeave;
        }

        private void VolumeStackPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            VolumePanel.MouseLeave -= VolumeStackPanel_MouseLeave;
            VolumeSlider.Visibility = Visibility.Collapsed;
            VolumePanel.Margin = new Thickness(0);
            VolumePanel.Background = new SolidColorBrush(Color.FromArgb(0, 34, 34, 34));
        }

        private void FullScreenButton_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Height = double.NaN;
            WideScreenButton.Visibility = Visibility.Visible;
            ScreenPanel.Margin = new Thickness(0, -33, 0, 0);
            ScreenPanel.Background = new SolidColorBrush(Color.FromArgb(255, 68, 68, 68));
            ScreenPanel.MouseLeave += ScreenStackPanel_MouseLeave;
        }

        private void ScreenStackPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            ScreenPanel.MouseLeave -= ScreenStackPanel_MouseLeave;
            WideScreenButton.Visibility = Visibility.Collapsed;
            ScreenPanel.Margin = new Thickness(0);
            ScreenPanel.Background = new SolidColorBrush(Color.FromArgb(0, 34, 34, 34));
        }

        #endregion
    }
}

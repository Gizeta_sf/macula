﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Gizeta.Macula.Utility;
using Gizeta.Macula.Video;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula播放器控件
    /// <para>继承UserControl类，实现Gizeta.Macula.Controls.IPlayer接口</para>
    /// </summary>
    public partial class Player : UserControl, IPlayer
    {
        private PlayerMediator mediator = PlayerMediator.Instance;
        
        public Player()
        {
            InitializeComponent();

            VideoPlayer.AutoPlay = PlayerConfig.Instance.AutoPlay;
            VideoPlayer.Volume = PlayerConfig.Instance.Volume;
        }

        #region IPlayer接口实现

        public event RoutedEventHandler BufferingProgressChanged;
        public event RoutedEventHandler CurrentStateChanged;
        public event RoutedEventHandler DownloadProgressChanged;
        public event TimelineMarkerRoutedEventHandler MarkerReached;
        public event RoutedEventHandler MediaEnded;
        public event EventHandler<ExceptionRoutedEventArgs> MediaFailed;
        public event RoutedEventHandler MediaOpened;
        public event RateChangedRoutedEventHandler RateChanged;

        public int AudioStreamCount
        {
            get { return VideoPlayer.AudioStreamCount; }
        }

        public int? AudioStreamIndex
        {
            get { return VideoPlayer.AudioStreamIndex; }
            set { VideoPlayer.AudioStreamIndex = value; }
        }

        public bool AutoPlay
        {
            get { return VideoPlayer.AutoPlay; }
            set { VideoPlayer.AutoPlay = value; }
        }

        public double Balance
        {
            get { return VideoPlayer.Balance; }
            set { VideoPlayer.Balance = value; }
        }

        public double BufferingProgress
        {
            get { return VideoPlayer.BufferingProgress; }
        }

        public TimeSpan BufferingTime
        {
            get { return VideoPlayer.BufferingTime; }
            set { VideoPlayer.BufferingTime = value; }
        }

        public bool CanPause
        {
            get { return VideoPlayer.CanPause; }
        }

        public bool CanSeek
        {
            get { return VideoPlayer.CanSeek; }
        }

        public MediaElementState CurrentState
        {
            get { return VideoPlayer.CurrentState; }
        }

        public double DownloadProgress
        {
            get { return VideoPlayer.DownloadProgress; }
        }

        public double DownloadProgressOffset
        {
            get { return VideoPlayer.DownloadProgressOffset; }
        }

        public double DroppedFramesPerSecond
        {
            get { return VideoPlayer.DroppedFramesPerSecond; }
        }

        public bool IsMuted
        {
            get { return VideoPlayer.IsMuted; }
            set { VideoPlayer.IsMuted = value; }
        }

        public TimelineMarkerCollection Markers
        {
            get { return VideoPlayer.Markers; }
        }

        public Duration NaturalDuration
        {
            get { return VideoPlayer.NaturalDuration; }
        }

        public int NaturalVideoHeight
        {
            get { return VideoPlayer.NaturalVideoHeight; }
        }

        public int NaturalVideoWidth
        {
            get { return VideoPlayer.NaturalVideoWidth; }
        }

        public double PlaybackRate
        {
            get { return VideoPlayer.PlaybackRate; }
            set { VideoPlayer.PlaybackRate = value; }
        }

        public TimeSpan Position
        {
            get { return VideoPlayer.Position; }
            set { VideoPlayer.Position = value; }
        }

        public double RenderedFramesPerSecond
        {
            get { return VideoPlayer.RenderedFramesPerSecond; }
        }

        public Uri Source
        {
            get { return VideoPlayer.Source; }
            set { VideoPlayer.Source = value; }
        }

        public Stretch Stretch
        {
            get { return VideoPlayer.Stretch; }
            set { VideoPlayer.Stretch = value; }
        }

        public double Volume
        {
            get { return VideoPlayer.Volume; }
            set { VideoPlayer.Volume = value; }
        }

        public IPlayer LoadVideo(VideoInfo videoInfo)
        {
            if (videoInfo.IsLocalFile)
            {
                VideoPlayer.SetSource(new LocalStreamContainer(videoInfo).Stream);
            }
            else
            {
                VideoPlayer.Source = new Uri(videoInfo.Url[0]);
            }
            return this;
        }

        public IPlayer Pause()
        {
            if (VideoPlayer.CanPause)
            {
                VideoPlayer.Pause();
            }
            return this;
        }

        public IPlayer Play()
        {
            VideoPlayer.Play();
            return this;
        }

        public IPlayer SetSource(MediaStreamSource source)
        {
            VideoPlayer.SetSource(source);
            return this;
        }

        public IPlayer SetSource(Stream source)
        {
            VideoPlayer.SetSource(source);
            return this;
        }

        public IPlayer Stop()
        {
            VideoPlayer.Stop();
            return this;
        }

        public void Initialize()
        {
            VideoPlayer.Source = null;
        }

        #endregion

        #region 私有事件处理

        private void VideoPlayer_BufferingProgressChanged(object sender, RoutedEventArgs e)
        {
            if (this.BufferingProgressChanged != null)
            {
                this.BufferingProgressChanged(this, e);
            }
        }

        private void VideoPlayer_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if (this.CurrentStateChanged != null)
            {
                this.CurrentStateChanged(this, e);
            }
        }

        private void VideoPlayer_DownloadProgressChanged(object sender, RoutedEventArgs e)
        {
            if (this.DownloadProgressChanged != null)
            {
                this.DownloadProgressChanged(this, e);
            }
        }

        private void VideoPlayer_MarkerReached(object sender, TimelineMarkerRoutedEventArgs e)
        {
            if (this.MarkerReached != null)
            {
                this.MarkerReached(this, e);
            }
        }

        private void VideoPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            VideoPlayer.Position = TimeSpan.Zero;
            if (PlayerConfig.Instance.IsLooped)
            {
                VideoPlayer.Play();
            }
            else
            {
                if (this.MediaEnded != null)
                {
                    this.MediaEnded(this, null);
                }
            }
        }

        private void VideoPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            if (this.MediaFailed != null)
            {
                this.MediaFailed(this, e);
            }
        }

        private void VideoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (this.MediaOpened != null)
            {
                this.MediaOpened(this, e);
            }
        }

        private void VideoPlayer_RateChanged(object sender, RateChangedRoutedEventArgs e)
        {
            if (this.RateChanged != null)
            {
                this.RateChanged(this, e);
            }
        }

        #endregion
    }
}

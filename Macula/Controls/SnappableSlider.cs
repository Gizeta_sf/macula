﻿/*
 * Reference: http://slvideoplayer.codeplex.com/
 * Author: jneubeck & timheuer
 * Modified by Gizeta
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// 可点击改值的滑动条。
    /// </summary>
    public class SnappableSlider : Slider
    {
        protected Thumb sliderThumb;
        private bool isInDrag = false;
        private FrameworkElement passedTrack;
        private FrameworkElement unpassedTrack;
        private double oldValue = 0, newValue = 0, prevNewValue = 0;
        public event RoutedPropertyChangedEventHandler<double> MyValueChanged;
        public event RoutedPropertyChangedEventHandler<double> MyValueChangedInDrag;
        private DispatcherTimer dragtimer = new DispatcherTimer();
        private double dragTimeElapsed = 0;
        private const short DragWaitThreshold = 200, DragWaitInterval = 100;
        private bool dragSeekJustFired = false;

        public SnappableSlider()
        {
            dragtimer.Interval = TimeSpan.FromMilliseconds(DragWaitInterval);
            dragtimer.Tick += dragtimer_Tick;
        }

        public bool IsInDrag
        {
            get { return isInDrag; }
        }

        private void dragtimer_Tick(object sender, EventArgs e)
        {
            dragTimeElapsed += DragWaitInterval;

            if (dragTimeElapsed >= DragWaitThreshold)
            {
                RoutedPropertyChangedEventHandler<double> handler = MyValueChangedInDrag;

                if ((handler != null) && (newValue != prevNewValue))
                {
                    handler(this, new RoutedPropertyChangedEventArgs<double>(oldValue, newValue));
                    dragSeekJustFired = true;
                    prevNewValue = newValue;
                }

                dragTimeElapsed = 0;
            }
        }

        private void CustomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            oldValue = e.OldValue;
            newValue = e.NewValue;

            if (sliderThumb.IsDragging)
            {
                dragTimeElapsed = 0;
                dragtimer.Stop();
                dragtimer.Start();
                dragSeekJustFired = false;
                isInDrag = true;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (Orientation == Orientation.Horizontal)
            {
                sliderThumb = GetTemplateChild("HorizontalThumb") as Thumb;
                passedTrack = GetTemplateChild("LeftTrack") as FrameworkElement;
                unpassedTrack = GetTemplateChild("RightTrack") as FrameworkElement;
            }
            else
            {
                sliderThumb = GetTemplateChild("VerticalThumb") as Thumb;
                passedTrack = GetTemplateChild("BottomTrack") as FrameworkElement;
                unpassedTrack = GetTemplateChild("TopTrack") as FrameworkElement;
            }

            if (passedTrack != null) passedTrack.MouseLeftButtonDown += OnMoveThumbToMouse;

            if (unpassedTrack != null) unpassedTrack.MouseLeftButtonDown += OnMoveThumbToMouse;

            sliderThumb.DragCompleted += DragCompleted;
            this.ValueChanged += CustomSlider_ValueChanged;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            Size s = base.ArrangeOverride(finalSize);

            if (double.IsNaN(sliderThumb.Width) && (sliderThumb.ActualWidth != 0))
            {
                sliderThumb.Width = sliderThumb.ActualWidth;
            }

            if (double.IsNaN(sliderThumb.Height) && (sliderThumb.ActualHeight != 0))
            {
                sliderThumb.Height = sliderThumb.ActualHeight;
            }

            if (double.IsNaN(sliderThumb.Width)) sliderThumb.Width = sliderThumb.Height;
            if (double.IsNaN(sliderThumb.Height)) sliderThumb.Height = sliderThumb.Width;

            return (s);
        }

        private void OnMoveThumbToMouse(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            Point p = e.GetPosition(this);

            if (this.Orientation == Orientation.Horizontal)
            {
                Value = (p.X - (sliderThumb.ActualWidth / 2)) / (ActualWidth - sliderThumb.ActualWidth) * Maximum;
            }
            else
            {
                Value = (1 - (p.Y - (sliderThumb.ActualHeight / 2)) / (ActualHeight - sliderThumb.ActualHeight)) * Maximum;
            }

            RoutedPropertyChangedEventHandler<double> handler = MyValueChanged;

            if (handler != null)
            {
                handler(this, new RoutedPropertyChangedEventArgs<double>(oldValue, Value));
            }
        }

        private void DragCompleted(object sender, DragCompletedEventArgs e)
        {
            dragtimer.Stop();
            dragTimeElapsed = 0;
            isInDrag = false;

            RoutedPropertyChangedEventHandler<double> handler = MyValueChanged;

            if ((handler != null) && (!dragSeekJustFired))
            {
                handler(this, new RoutedPropertyChangedEventArgs<double>(oldValue, this.Value));
            }
        }
    }
}
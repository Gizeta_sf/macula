﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System.IO;

namespace Gizeta.Macula.Video
{
    public class LocalStreamContainer
    {
        FileStream stream;

        public Stream Stream
        {
            get { return stream; }
        }

        public LocalStreamContainer(VideoInfo videoInfo)
        {
            stream = new FileStream(videoInfo.Url[0], FileMode.Open);
        }
    }
}

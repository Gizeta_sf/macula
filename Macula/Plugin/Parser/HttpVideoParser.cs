﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.Windows;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Video;

namespace Gizeta.Macula.Plugin.Parser
{
    /// <summary>
    /// 网络视频解析工具
    /// <para>继承Gizeta.Macula.Plugin.Parser.IVideoParser接口</para>
    /// </summary>
    public class HttpVideoParser : IVideoParser
    {
        public event EventHandler<ParseCompletedEventArgs> ParseCompleted;
        public event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;

        public IPluginInfo PluginInfo
        {
            get { return new HttpVideoParserInfo(); }
        }

        public void Parse(Dictionary<string, string> data)
        {
            if (data == null)
            {
                if (this.Erred != null)
                {
                    this.Erred(this, new ApplicationUnhandledExceptionEventArgs(new Exception("解析数据为空"), false));
                }
                return;
            }

            string vid;
            if (!data.TryGetValue("vid", out vid))
            {
                if (this.Erred != null)
                {
                    this.Erred(this, new ApplicationUnhandledExceptionEventArgs(new Exception("视频地址为空"), false));
                }
                return;
            }

            if (this.ParseCompleted != null)
            {
                this.ParseCompleted(this, new ParseCompletedEventArgs(result: new VideoInfo(url: new List<string>() { vid }, length: new List<int>() { -1 }, totalLength: -1)));
            }
        }
    }
}

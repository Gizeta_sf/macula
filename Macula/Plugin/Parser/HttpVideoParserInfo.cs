﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Plugin.Parser
{
    public class HttpVideoParserInfo : IVideoParserInfo
    {
        public string TargetType
        {
            get { return "http"; }
        }

        public string Name
        {
            get { return "Macula.Http"; }
        }

        public string Version
        {
            get { return "r5"; }
        }

        public string Author
        {
            get { return "Gizeta"; }
        }

        public string Website
        {
            get { return ""; }
        }

        public string FullName
        {
            get { return "Gizeta.Macula.Plugin.Parser.HttpVideoParser"; }
        }

        public Type InstanceType
        {
            get { return typeof(HttpVideoParser); }
        }

        public PluginType Type
        {
            get { return PluginType.VideoParser; }
        }
    }
}

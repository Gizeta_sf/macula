﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;

using Gizeta.Macula.Plugin;
using Gizeta.Macula.Plugin.Parser;
using Gizeta.Macula.Plugin.Player;
using Gizeta.Macula.Utility;

namespace Gizeta.Macula
{
    public partial class App : Application
    {
        private PlayerMediator mediator = PlayerMediator.Instance;
        private PlayerConfig config = PlayerConfig.Instance;
        private Dictionary<string, string> initParams;
        
        public App()
        {
            if (App.Current.InstallState == InstallState.Installed)
            {
                App.Current.CheckAndDownloadUpdateCompleted += App_CheckAndDownloadUpdateCompleted;
                App.Current.CheckAndDownloadUpdateAsync();
            }
            
            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
            this.UnhandledException += this.Application_UnhandledException;

            InitializeComponent();
        }

        private void App_CheckAndDownloadUpdateCompleted(object sender, CheckAndDownloadUpdateCompletedEventArgs e)
        {
            if (e.UpdateAvailable && e.Error == null)
            {
                MessageBox.Show("应用新版本已经下载成功，将在下次启动时生效。");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("在检测应用更新时, 出现以下错误信息:" + Environment.NewLine + e.Error.Message);
                Logger.Log(new Exception("更新失败", e.Error));
            }
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            this.RootVisual = new MainPage();
            if (!App.Current.IsRunningOutOfBrowser)
            {
                if (!App.Current.HasElevatedPermissions)
                {
                    PlayerMediator.Instance.NotificationPanel.AddMessage("提升权限失败。");
                }
            }
            PluginInfoManager.Instance.AddAssembly("http", new HttpVideoParserInfo());
            PluginInfoManager.Instance.AddAssembly("local", new LocalVideoParserInfo());
            initParams = new Dictionary<string, string>(App.Current.Host.InitParams);
            initParams["firstRun"] = "true";
            config.LoadCompleted += config_LoadCompleted;
            string configUrl;
            if (initParams.TryGetValue("config", out configUrl))
            {
                config.Load(new Uri(configUrl, configUrl.Contains("://") ? UriKind.Absolute : UriKind.Relative));
            }
            else
            {
                config.Load(new Uri("config.xml", UriKind.Relative));
            }
        }

        private void config_LoadCompleted(object sender, EventArgs e)
        {
            if (!App.Current.IsRunningOutOfBrowser)
            {
                mediator.PlayVideo(initParams);
            }
            else
            {
                mediator.InitializeAllControls();
                PlayerPluginAssemblyManager.Instance.InitializeAllPlugin(true);
            }
        }

        private void Application_Exit(object sender, EventArgs e)
        {
            //保存设置
        }

        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // 如果应用程序是在调试器外运行的，则使用浏览器的
            // 异常机制报告该异常。在 IE 上，将在状态栏中用一个 
            // 黄色警报图标来显示该异常，而 Firefox 则会显示一个脚本错误。
            if (!System.Diagnostics.Debugger.IsAttached)
            {

                // 注意: 这使应用程序可以在已引发异常但尚未处理该异常的情况下
                // 继续运行。 
                // 对于生产应用程序，此错误处理应替换为向网站报告错误
                // 并停止应用程序。
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
            }
        }

        private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
            }
            catch (Exception)
            {
            }
        }
    }
}

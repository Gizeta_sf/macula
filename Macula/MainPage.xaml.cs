﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Utility;

namespace Gizeta.Macula
{
    public partial class MainPage : UserControl, IMainPage
    {
        private Storyboard slideStoryBoard;
        private PlayerConfig config = PlayerConfig.Instance;
        private PlayerMediator mediator = PlayerMediator.Instance;
        private double controlBarOriginalHeight = 33;
        private double configPageOriginalWidth = 345;
        
        public MainPage()
        {
            InitializeComponent();
            mediator.MainPage = this;
            App.Current.Host.Content.FullScreenChanged += Content_FullScreenChanged;
        }

        public void SetFullScreenStyle()
        {
            if (App.Current.Host.Content.IsFullScreen)
            {
                PlayerConfig.Instance.IsFullScreen = true;
                
                ConfigPage.Visibility = Visibility.Collapsed;

                SplineDoubleKeyFrame frame1 = new SplineDoubleKeyFrame() { KeyTime = TimeSpan.Zero, Value = 0 };
                EasingDoubleKeyFrame frame2 = new EasingDoubleKeyFrame() { KeyTime = TimeSpan.FromSeconds(0.5), Value = controlBarOriginalHeight, EasingFunction = new QuinticEase() { EasingMode = EasingMode.EaseInOut } };
                SplineDoubleKeyFrame frame3 = new SplineDoubleKeyFrame() { KeyTime = TimeSpan.FromSeconds(3.5), Value = controlBarOriginalHeight };
                EasingDoubleKeyFrame frame4 = new EasingDoubleKeyFrame() { KeyTime = TimeSpan.FromSeconds(4), Value = 0, EasingFunction = new QuinticEase() { EasingMode = EasingMode.EaseInOut } };
                DoubleAnimationUsingKeyFrames slideAnimation = new DoubleAnimationUsingKeyFrames() { KeyFrames = { frame1, frame2, frame3, frame4 } };

                slideStoryBoard = new Storyboard() { Children = { slideAnimation } };
                Storyboard.SetTarget(slideAnimation, ControlBar);
                Storyboard.SetTargetProperty(slideAnimation, new PropertyPath("Height"));
                slideStoryBoard.Begin();

                PlayerContainer.MouseMove += playerContainer_MouseMove;
                ControlBar.MouseEnter += controlBar_MouseEnter;

                PlayerContainer.Focus();
            }
            else
            {
                PlayerConfig.Instance.IsFullScreen = false;

                PlayerContainer.MouseMove -= playerContainer_MouseMove;
                ControlBar.MouseEnter -= controlBar_MouseEnter;
                ControlBar.MouseLeave -= controlBar_MouseLeave;
                slideStoryBoard.Stop();
                slideStoryBoard = null;

                ControlBar.Height = controlBarOriginalHeight;
                ConfigPage.Visibility = Visibility.Visible;
            }
        }

        public void SetWideScreenStyle()
        {
            if (config.IsWideScreen)
            {
                if (config.IsFullScreen)
                {
                    ConfigPage.Width = 0;
                }
                else
                {
                    DoubleAnimation slideAnimation = new DoubleAnimation() { From = configPageOriginalWidth, To = 0, EasingFunction = new QuinticEase() { EasingMode = EasingMode.EaseOut }, Duration = TimeSpan.FromSeconds(1), BeginTime = TimeSpan.FromSeconds(0) };
                    slideStoryBoard = new Storyboard() { Children = { slideAnimation } };
                    Storyboard.SetTarget(slideAnimation, ConfigPage);
                    Storyboard.SetTargetProperty(slideAnimation, new PropertyPath("Width"));
                    slideStoryBoard.Completed += slideStoryBoard_Completed;
                    slideStoryBoard.Begin();
                }
            }
            else
            {
                if (config.IsFullScreen)
                {
                    ConfigPage.Width = configPageOriginalWidth;
                }
                else
                {
                    DoubleAnimation slideAnimation = new DoubleAnimation() { From = 0, To = configPageOriginalWidth, EasingFunction = new QuinticEase() { EasingMode = EasingMode.EaseOut }, Duration = TimeSpan.FromSeconds(1), BeginTime = TimeSpan.FromSeconds(0) };
                    slideStoryBoard = new Storyboard() { Children = { slideAnimation } };
                    Storyboard.SetTarget(slideAnimation, ConfigPage);
                    Storyboard.SetTargetProperty(slideAnimation, new PropertyPath("Width"));
                    slideStoryBoard.Completed += slideStoryBoard_Completed;
                    slideStoryBoard.Begin();
                }
            }
        }

        public void ChangeConfigPageOriginalWidth(double value)
        {
            configPageOriginalWidth = value;
        }

        public void ChangeControlBarOriginalHeight(double value)
        {
            controlBarOriginalHeight = value;
        }

        private void playerContainer_MouseMove(object sender, MouseEventArgs e)
        {
            if (slideStoryBoard.GetCurrentTime() > TimeSpan.FromSeconds(3))
            {
                slideStoryBoard.Seek(TimeSpan.FromSeconds(4) - slideStoryBoard.GetCurrentTime());
            }
            else if (slideStoryBoard.GetCurrentTime() > TimeSpan.FromSeconds(1))
            {
                slideStoryBoard.Seek(TimeSpan.FromSeconds(1));
            }
        }

        private void controlBar_MouseEnter(object sender, MouseEventArgs e)
        {
            ControlBar.MouseLeave += controlBar_MouseLeave;
            ControlBar.MouseEnter -= controlBar_MouseEnter;
            PlayerContainer.MouseMove -= playerContainer_MouseMove;

            slideStoryBoard.Seek(TimeSpan.FromSeconds(0.5));
            slideStoryBoard.Pause();
        }

        private void controlBar_MouseLeave(object sender, MouseEventArgs e)
        {
            ControlBar.MouseLeave -= controlBar_MouseLeave;
            ControlBar.MouseEnter += controlBar_MouseEnter;
            PlayerContainer.MouseMove += playerContainer_MouseMove;

            slideStoryBoard.Resume();
        }

        private void slideStoryBoard_Completed(object sender, EventArgs e)
        {
            slideStoryBoard.Completed -= slideStoryBoard_Completed;
        }

        private void Content_FullScreenChanged(object sender, EventArgs e)
        {
            SetFullScreenStyle();
        }
    }
}

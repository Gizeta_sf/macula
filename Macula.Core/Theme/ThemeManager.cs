﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.Windows;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Theme
{
    /// <summary>
    /// 主题管理者
    /// <para>该类不可继承</para>
    /// </summary>
    public sealed class ThemeManager
    {
        private Dictionary<string, Uri> themeDictionary = new Dictionary<string, Uri>();

        private static readonly ThemeManager instance = new ThemeManager();

        private ThemeManager()
        {
        }

        public static ThemeManager Instance
        {
            get { return instance; }
        }

        public void AddTheme(string name, Uri url)
        {
            themeDictionary[name] = url;
        }

        public void ApplyTheme(string name)
        {
            if (name == "default")
            {
                ResourceDictionary rd = new ResourceDictionary();
                rd.Source = new Uri("/Macula;Component/Theme/DefaultTheme.xaml");
                Application.Current.Resources.MergedDictionaries.Add(rd);
            }
            else if (themeDictionary.ContainsKey(name))
            {
                ResourceDictionary rd = new ResourceDictionary();
                rd.Source = themeDictionary[name];
                Application.Current.Resources.MergedDictionaries.Add(rd);
            }
            else Logger.Log(new Exception("未找到主题:" + name));
        }
    }
}

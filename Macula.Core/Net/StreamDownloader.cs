﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;
using System.Net;
using System.Windows;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 文件流下载工具
    /// <para>继承Gizeta.Macula.Net.SimpleDownloader类</para>
    /// </summary>
    public class StreamDownloader : SimpleDownloader
    {
        public override void Download(Uri url)
        {
            webClient = new WebClient();
            isDownloading = true;
            webClient.AllowReadStreamBuffering = true; //为了主线程读取，设为true，同时DownloadProgressChanged事件失效
            webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
            webClient.OpenReadCompleted += webClient_OpenReadCompleted;
            webClient.OpenReadAsync(url);
        }

        private void webClient_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            onDownloadProgressChanged(sender, new DownloadProgressChangedEventArgs(e.BytesReceived, e.TotalBytesToReceive));
        }

        private void webClient_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            isDownloading = false;
            if (e.Cancelled)
            {
                return;
            }
            if (e.Error != null)
            {
                onErred(sender, new ApplicationUnhandledExceptionEventArgs(new Exception("下载失败", e.Error), false));
            }
            else
            {
                onDownloadCompleted(sender, new DownloadCompletedEventArgs(e.Result));
            }
            webClient.OpenReadCompleted -= webClient_OpenReadCompleted;
            webClient.DownloadProgressChanged -= webClient_DownloadProgressChanged;
        }
    }
}

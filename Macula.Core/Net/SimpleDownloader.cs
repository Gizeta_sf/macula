﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Net;
using System.Windows;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 简单下载工具的抽象类
    /// <para>不支持渐进式下载</para>
    /// <para>实现Gizeta.Macula.Net.IDownloader接口</para>
    /// </summary>
    public abstract class SimpleDownloader : IDownloader
    {
        public event EventHandler<DownloadCompletedEventArgs> DownloadCompleted;
        public event EventHandler<DownloadProgressChangedEventArgs> DownloadProgressChanged;
        public event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;
        
        protected WebClient webClient;
        protected bool isDownloading = false;

        public abstract void Download(Uri url);

        public void Cancel()
        {
            if (isDownloading)
            {
                if (webClient.IsBusy)
                {
                    webClient.CancelAsync();
                }
            }
        }

        protected void onDownloadCompleted(object sender, DownloadCompletedEventArgs e)
        {
            if (this.DownloadCompleted != null)
            {
                this.DownloadCompleted(this, e);
            }
        }

        protected void onErred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (this.Erred != null)
            {
                this.Erred(this, e);
            }
        }

        protected void onDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (this.DownloadProgressChanged != null)
            {
                this.DownloadProgressChanged(this, e);
            }
        }
    }
}

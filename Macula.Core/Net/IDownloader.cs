﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Windows;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 下载工具接口
    /// </summary>
    public interface IDownloader
    {
        void Download(Uri url);

        void Cancel();

        event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;
        event EventHandler<DownloadCompletedEventArgs> DownloadCompleted;
        event EventHandler<DownloadProgressChangedEventArgs> DownloadProgressChanged;
    }
}

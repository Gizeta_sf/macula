﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;
using System.Net;
using System.Windows;

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// HTTP渐进式下载工具
    /// <para>实现Gizeta.Macula.Net.IDownloader接口</para>
    /// </summary>
    public class HttpProgressiveDownloader : IDownloader
    {
        public event EventHandler<DownloadCompletedEventArgs> DownloadCompleted;
        public event EventHandler<DownloadProgressChangedEventArgs> DownloadProgressChanged;
        public event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;

        private HttpWebRequest httpWebRequest;

        public void Download(Uri url)
        {
            try
            {
                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.AllowReadStreamBuffering = false;
                httpWebRequest.BeginGetResponse(new AsyncCallback(responseCallback), null);
            }
            catch (Exception e)
            {
                if (this.Erred != null)
                {
                    this.Erred(this, new ApplicationUnhandledExceptionEventArgs(new Exception("WebRequest连接失败", e), false));
                }
            }
        }

        public void Cancel()
        {
            httpWebRequest.Abort();
        }

        private void responseCallback(IAsyncResult asynchronousResult)
        {
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.EndGetResponse(asynchronousResult);

            Stream responseStream = httpWebResponse.GetResponseStream();
            MemoryStream memoryStream = new MemoryStream();
            long bytesReceived = 0;
            long totalBytes = httpWebResponse.ContentLength;
            byte[] bytes = new byte[1024];
            int size = responseStream.Read(bytes, 0, bytes.Length);
            while (size > 0)
            {
                bytesReceived += size;
                memoryStream.Write(bytes, 0, size);
                if (this.DownloadProgressChanged != null)
                {
                    this.DownloadProgressChanged(this, new DownloadProgressChangedEventArgs(bytesReceived, totalBytes, memoryStream));
                }
                size = responseStream.Read(bytes, 0, bytes.Length);
            }

            responseStream.Close();
            if (this.DownloadCompleted != null)
            {
                this.DownloadCompleted(this, new DownloadCompletedEventArgs(memoryStream));
            }
        }
    }
}

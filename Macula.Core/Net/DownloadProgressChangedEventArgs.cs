﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 下载进度改变的事件数据
    /// <para>继承EventArgs类</para>
    /// </summary>
    public class DownloadProgressChangedEventArgs : EventArgs
    {
        private long bytesReceived;
        private long totalBytes;
        private Stream streamReceived;

        public double DownloadProgress
        {
            get { return bytesReceived / totalBytes; }
        }

        public Stream StreamReceived
        {
            get { return streamReceived; }
        }

        public DownloadProgressChangedEventArgs(long bytesReceived, long totalBytes, Stream streamReceived = null)
        {
            this.bytesReceived = bytesReceived;
            this.totalBytes = totalBytes;
            this.streamReceived = streamReceived;
        }
    }
}

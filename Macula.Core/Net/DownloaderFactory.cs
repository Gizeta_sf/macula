﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 下载工具工厂类
    /// </summary>
    public class DownloaderFactory
    {
        private static Dictionary<string, Type> downloaderTypeList = new Dictionary<string, Type>() { { "StreamDownloader", typeof(StreamDownloader) }, { "StringDownloader", typeof(StringDownloader) }, { "HttpProgressiveDownloader", typeof(HttpProgressiveDownloader) } };

        public static void RegisterDownloader(string name, Type type)
        {
            if (typeof(IDownloader).IsAssignableFrom(type))
            {
                downloaderTypeList[name] = type;
            }
        }

        public static IDownloader GetDownloader(string name)
        {
            if (downloaderTypeList.ContainsKey(name))
            {
                return Activator.CreateInstance(downloaderTypeList[name]) as IDownloader;
            }
            else
            {
                return null;
            }
        }
    }
}

﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 下载完成的事件数据
    /// <para>继承EventArgs类</para>
    /// </summary>
    public class DownloadCompletedEventArgs : EventArgs
    {
        private object result;

        public object Result
        {
            get { return result; }
        }

        public DownloadCompletedEventArgs(object result)
        {
            this.result = result;
        }
    }
}

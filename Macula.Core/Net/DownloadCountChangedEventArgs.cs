﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

namespace Gizeta.Macula.Net
{
    /// <summary>
    /// 下载数量改变的事件数据
    /// <para>继承System.EventArgs类</para>
    /// </summary>
    public class DownloadCountChangedEventArgs : EventArgs
    {
        private int currentDownloaded = 0;

        public int CurrentDownloaded
        {
            get { return currentDownloaded; }
        }

        private int totalDownload = 0;

        public int TotalDownload
        {
            get { return totalDownload; }
        }

        public DownloadCountChangedEventArgs(int currentDownloaded, int totalDownload)
        {
            this.currentDownloaded = currentDownloaded;
            this.totalDownload = totalDownload;
        }
    }
}

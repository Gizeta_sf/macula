﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Xml.Linq;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Net;
using Gizeta.Macula.Plugin.Player;

namespace Gizeta.Macula.Plugin
{
    /// <summary>
    /// 插件加载类
    /// </summary>
    public class PluginLoader
    {
        public event EventHandler<LoadCompletedEventArgs> LoadCompleted;
        public event EventHandler<DownloadCountChangedEventArgs> DownloadCountChanged;

        private int currentDownloaded = 0;
        private int totalDownload = 0;
        private int wrongLoaded = 0;

        List<Uri> pluginList = new List<Uri>();

        public void Load(XElement xml)
        {
            if (xml.Element("VideoParser") != null)
            {
                foreach (XElement xe in xml.Element("VideoParser").Descendants("item"))
                {
                    Uri url;
                    url = new Uri(xe.Value, xe.Value.Contains("://") ? UriKind.Absolute : UriKind.Relative);
                    pluginList.Add(url);
                }
            }
            if (xml.Element("PlayerPlugin") != null)
            {
                foreach (XElement xe in xml.Element("PlayerPlugin").Descendants("item"))
                {
                    Uri url;
                    url = new Uri(xe.Value, xe.Value.Contains("://") ? UriKind.Absolute : UriKind.Relative);
                    pluginList.Add(url);
                }
            }
            loadPluginList(pluginList);
        }

        private void loadPluginList(List<Uri> plugin)
        {
            totalDownload = plugin.Count;

            if (totalDownload == 0)
            {
                if (this.LoadCompleted != null)
                {
                    this.LoadCompleted(this, new LoadCompletedEventArgs(0, 0));
                }
                return;
            }

            if (this.DownloadCountChanged != null)
            {
                this.DownloadCountChanged(this, new DownloadCountChangedEventArgs(currentDownloaded, totalDownload));
            }

            foreach (Uri url in plugin)
            {
                IDownloader downloader = DownloaderFactory.GetDownloader("StreamDownloader");
                downloader.Erred += downloader_Erred;
                downloader.DownloadCompleted += downloader_PluginDownloadCompleted;
                downloader.Download(url);
            }
        }

        private void downloader_PluginDownloadCompleted(object sender, DownloadCompletedEventArgs e)
        {
            IDownloader downloader = sender as IDownloader;
            downloader.Erred -= downloader_Erred;
            downloader.DownloadCompleted -= downloader_PluginDownloadCompleted;
            downloader = null;

            AssemblyPart assemPart = new AssemblyPart();
            try
            {
                Assembly assem = assemPart.Load((Stream)e.Result);
                Type[] types = assem.GetExportedTypes();
                Type pluginInfoType = typeof(IPluginInfo);
                foreach (Type typeClass in types)
                {
                    if (!typeClass.IsAbstract && pluginInfoType.IsAssignableFrom(typeClass))
                    {
                        IPluginInfo pluginInfo = Activator.CreateInstance(typeClass) as IPluginInfo;
                        PluginInfoManager.Instance.AddAssembly(pluginInfo.FullName, pluginInfo);
                        /*if (pluginInfo.Type == PluginType.PlayerPlugin)
                        {
                            IPlayerPlugin playerPlugin = Activator.CreateInstance(pluginInfo.InstanceType) as IPlayerPlugin;
                            if (playerPlugin != null)
                            {
                                playerPlugin.Execute(MainPage.Instance.InitParams);
                            }
                        }*/
                    }
                }
                
            }
            catch
            {
                wrongLoaded++;
            }
            finally
            {
                currentDownloaded++;
            }

            if (this.DownloadCountChanged != null)
            {
                this.DownloadCountChanged(this, new DownloadCountChangedEventArgs(currentDownloaded, totalDownload));
            }

            if (currentDownloaded == totalDownload)
            {
                if (this.LoadCompleted != null)
                {
                    this.LoadCompleted(this, new LoadCompletedEventArgs(wrongLoaded, totalDownload));
                }
            }
        }

        private void downloader_Erred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            IDownloader downloader = sender as IDownloader;
            downloader.Erred -= downloader_Erred;
            downloader.DownloadCompleted -= downloader_PluginDownloadCompleted;
            downloader = null;

            currentDownloaded++;
            wrongLoaded++;

            if (this.DownloadCountChanged != null)
            {
                this.DownloadCountChanged(this, new DownloadCountChangedEventArgs(currentDownloaded, totalDownload));
            }
            if (currentDownloaded == totalDownload)
            {
                if (this.LoadCompleted != null)
                {
                    this.LoadCompleted(this, new LoadCompletedEventArgs(wrongLoaded, totalDownload));
                }
            }
        }
    }
}

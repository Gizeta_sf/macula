﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

namespace Gizeta.Macula.Plugin
{
    /// <summary>
    /// 插件加载完成的事件数据
    /// <para>继承System.EventArgs类</para>
    /// </summary>
    public class LoadCompletedEventArgs : EventArgs
    {
        private int wrongLoaded = 0;

        public int WrongLoaded
        {
            get { return wrongLoaded; }
        }

        private int totalLoaded = 0;

        public int TotalLoaded
        {
            get { return totalLoaded; }
        }

        public LoadCompletedEventArgs(int wrongLoaded, int totalLoaded)
        {
            this.wrongLoaded = wrongLoaded;
            this.totalLoaded = totalLoaded;
        }
    }
}

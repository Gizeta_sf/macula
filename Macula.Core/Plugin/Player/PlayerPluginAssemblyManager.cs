﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System.Collections.Generic;

namespace Gizeta.Macula.Plugin.Player
{
    /// <summary>
    /// 播放器插件程序集管理者
    /// </summary>
    public class PlayerPluginAssemblyManager
    {
        private static readonly PlayerPluginAssemblyManager instance = new PlayerPluginAssemblyManager();
        private List<IPlayerPlugin> pluginList = new List<IPlayerPlugin>();

        private PlayerPluginAssemblyManager()
        {
        }

        public static PlayerPluginAssemblyManager Instance
        {
            get { return instance; }
        }

        public void AddPlugin(IPlayerPlugin plugin)
        {
            pluginList.Add(plugin);
        }

        public void InitializeAllPlugin(bool firstRun = false)
        {
            foreach (var plugin in pluginList)
            {
                plugin.Initialize(firstRun);
            }
        }

        public void LoadPlugins(Dictionary<string, string> param, bool firstRun = false)
        {
            foreach (var plugin in pluginList)
            {
                if (firstRun || !plugin.IsRunOnce)
                {
                    plugin.Execute(param);
                }
            }
        }

        public void UnloadPlugin(IPlayerPlugin plugin)
        {
            plugin.Unload();
            pluginList.Remove(plugin);
        }
    }
}

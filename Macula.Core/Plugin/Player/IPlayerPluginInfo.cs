﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Plugin.Player
{
    public interface IPlayerPluginInfo : IPluginInfo
    {
        string Description { get; }
    }
}

﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System.Collections.Generic;

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Plugin.Player
{
    /// <summary>
    /// Macula播放器插件接口
    /// <para>继承Gizeta.Macula.ComponentModel.IPlugin接口</para>
    /// </summary>
    public interface IPlayerPlugin : IPlugin
    {
        bool IsRunOnce { get; }
        
        void Execute(Dictionary<string, string> param);
        void Initialize(bool isFirstRun = false);
        void Unload();
    }
}

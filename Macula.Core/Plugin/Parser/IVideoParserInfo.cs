﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Plugin.Parser
{
    public interface IVideoParserInfo : IPluginInfo
    {
        string TargetType { get; }
    }
}

﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.Windows;

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Plugin.Parser
{
    /// <summary>
    /// 视频解析接口
    /// <para>继承Gizeta.Macula.ComponentModel.IPlugin接口</para>
    /// </summary>
    public interface IVideoParser : IPlugin
    {
        event EventHandler<ParseCompletedEventArgs> ParseCompleted;
        event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;

        void Parse(Dictionary<string, string> param);
    }
}

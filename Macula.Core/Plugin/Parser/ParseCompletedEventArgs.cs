﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;

using Gizeta.Macula.Video;

namespace Gizeta.Macula.Plugin.Parser
{
    /// <summary>
    /// 解析完成事件的数据
    /// <para>继承System.EventArgs类</para>
    /// </summary>
    public class ParseCompletedEventArgs : EventArgs
    {
        private VideoInfo result;
        public VideoInfo Result
        {
            get { return result; }
        }

        private string title = "";
        public string Title
        {
            get { return title; }
        }

        private string description = "";
        public string Description
        {
            get { return description; }
        }

        private string author = "";
        public string Author
        {
            get { return author; }
        }

        private List<string> tag = null;
        public List<string> Tag
        {
            get { return tag; }
        }

        private string partName = "";
        public string PartName
        {
            get { return partName; }
        }

        public ParseCompletedEventArgs(VideoInfo result, string title = "", string description = "", string author = "", List<string> tag = null, string partName = "")
        {
            this.result = result;
            this.title = title;
            this.description = description;
            this.author = author;
            this.tag = tag;
            this.partName = partName;
        }
    }
}

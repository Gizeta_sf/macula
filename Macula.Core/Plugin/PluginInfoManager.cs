﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Plugin.Parser;
using Gizeta.Macula.Plugin.Player;
using Gizeta.Macula.Utility;

namespace Gizeta.Macula.Plugin
{
    /// <summary>
    /// 插件信息集管理者
    /// </summary>
    public class PluginInfoManager
    {
        private Dictionary<string, IVideoParserInfo> parserDictionary = new Dictionary<string, IVideoParserInfo>();
        private Dictionary<string, IPlayerPluginInfo> pluginDictionary = new Dictionary<string, IPlayerPluginInfo>();

        private static readonly PluginInfoManager instance = new PluginInfoManager();

        private PluginInfoManager()
        {
        }

        public List<IVideoParserInfo> VideoParserInfoList
        {
            get { return new List<IVideoParserInfo>(parserDictionary.Values); }
        }

        public List<IPlayerPluginInfo> PlayerPluginInfoList
        {
            get { return new List<IPlayerPluginInfo>(pluginDictionary.Values); }
        }

        public static PluginInfoManager Instance
        {
            get { return instance; }
        }

        public IPlugin GetPlugin(string name, PluginType type)
        {
            switch (type)
            {
                case PluginType.PlayerPlugin:
                    if (pluginDictionary.ContainsKey(name))
                    {
                        return Activator.CreateInstance(pluginDictionary[name].InstanceType) as IPlugin;
                    }
                    else
                    {
                        return null;
                    }
                case PluginType.VideoParser:
                    if (parserDictionary.ContainsKey(name))
                    {
                        return Activator.CreateInstance(parserDictionary[name].InstanceType) as IPlugin;
                    }
                    else
                    {
                        return null;
                    }
                default:
                    return null;
            }
        }

        public void AddAssembly(string key, IPluginInfo value)
        {
            switch (value.Type)
            {
                case PluginType.PlayerPlugin:
                    IPlayerPluginInfo playerPluginInfo = value as IPlayerPluginInfo;
                    if (playerPluginInfo != null)
                    {
                        IPlayerPlugin plugin = Activator.CreateInstance(playerPluginInfo.InstanceType) as IPlayerPlugin;
                        if (plugin != null)
                        {
                            pluginDictionary.Add(playerPluginInfo.FullName, playerPluginInfo);
                            PlayerPluginAssemblyManager.Instance.AddPlugin(plugin);
                        }
                        else
                        {
                            Logger.Log(new Exception("播放器插件载入失败:" + value.FullName));
                        }
                    }
                    else
                    {
                        Logger.Log(new Exception("未知的播放器插件信息:" + value.FullName));
                    }
                    break;
                case PluginType.VideoParser:
                    IVideoParserInfo videoParserInfo = value as IVideoParserInfo;
                    if (videoParserInfo != null)
                    {
                        IVideoParser parser = Activator.CreateInstance(videoParserInfo.InstanceType) as IVideoParser;
                        if (parser != null)
                        {
                            parserDictionary.Add(videoParserInfo.TargetType, videoParserInfo);
                        }
                        else
                        {
                            Logger.Log(new Exception("视频解析插件载入失败:" + value.FullName));
                        }
                    }
                    else
                    {
                        Logger.Log(new Exception("未知的视频解析插件信息:" + value.FullName));
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

namespace Gizeta.Macula.Plugin
{
    public enum PluginType
    {
        VideoParser,
        PlayerPlugin
    }
}

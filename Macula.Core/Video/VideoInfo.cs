﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;

namespace Gizeta.Macula.Video
{
    /// <summary>
    /// 视频解析信息
    /// </summary>
    public class VideoInfo
    {
        private List<string> url = null;
        public List<string> Url
        {
            get { return url; }
        }

        private List<int> length = null;
        public List<int> Length
        {
            get { return length; }
        }

        private int totalLength = 0;
        public int TotalLength
        {
            get { return totalLength; }
        }

        private bool isLocalFile = false;
        public bool IsLocalFile
        {
            get { return isLocalFile; }
        }

        public VideoInfo(List<string> url, List<int> length, int totalLength, bool isLocalFile = false)
        {
            this.url = url;
            this.length = length;
            this.totalLength = totalLength;
            this.isLocalFile = isLocalFile;
        }
    }
}

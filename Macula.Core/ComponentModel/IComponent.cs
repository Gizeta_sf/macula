﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using Gizeta.Macula.Utility;

namespace Gizeta.Macula.ComponentModel
{
    /// <summary>
    /// Macula播放器组件接口
    /// </summary>
    public interface IComponent : IControl
    {
        void Initialize();
    }
}

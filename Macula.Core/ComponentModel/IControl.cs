﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Threading;

namespace Gizeta.Macula.ComponentModel
{
    /// <summary>
    /// Macula控件接口
    /// </summary>
    public interface IControl
    {
        #region UserControl部分属性

        double ActualHeight { get; }
        double ActualWidth { get; }
        bool AllowDrop { get; set; }
        Brush Background { get; set; }
        Brush BorderBrush { get; set; }
        Thickness BorderThickness { get; set; }
        CacheMode CacheMode { get; set; }
        int CharacterSpacing { get; set; }
        Geometry Clip { get; set; }
        UIElement Content { get; set; }
        Cursor Cursor { get; set; }
        Size DesiredSize { get; }
        Dispatcher Dispatcher { get; }
        Effect Effect { get; set; }
        FlowDirection FlowDirection { get; set; }
        FontFamily FontFamily { get; set; }
        double FontSize { get; set; }
        FontStretch FontStretch { get; set; }
        FontStyle FontStyle { get; set; }
        FontWeight FontWeight { get; set; }
        Brush Foreground { get; set; }
        double Height { get; set; }
        HorizontalAlignment HorizontalAlignment { get; set; }
        HorizontalAlignment HorizontalContentAlignment { get; set; }
        bool IsEnabled { get; set; }
        bool IsHitTestVisible { get; set; }
        bool IsTabStop { get; set; }
        Thickness Margin { get; set; }
        double MaxHeight { get; set; }
        double MaxWidth { get; set; }
        double MinHeight { get; set; }
        double MinWidth { get; set; }
        double Opacity { get; set; }
        Brush OpacityMask { get; set; }
        Thickness Padding { get; set; }
        DependencyObject Parent { get; }
        Projection Projection { get; set; }
        Size RenderSize { get; }
        Transform RenderTransform { get; set; }
        Point RenderTransformOrigin { get; set; }
        Style Style { get; set; }
        int TabIndex { get; set; }
        KeyboardNavigationMode TabNavigation { get; set; }
        ControlTemplate Template { get; set; }
        TriggerCollection Triggers { get; }
        bool UseLayoutRounding { get; set; }
        VerticalAlignment VerticalAlignment { get; set; }
        VerticalAlignment VerticalContentAlignment { get; set; }
        Visibility Visibility { get; set; }
        double Width { get; set; }

        #endregion

        #region UserControl部分函数

        void AddHandler(RoutedEvent routedEvent, Delegate handler, bool handledEventsToo);
        bool ApplyTemplate();
        void Arrange(Rect finalRect);
        bool CaptureMouse();
        bool CheckAccess();
        object FindName(string name);
        bool Focus();
        void InvalidateArrange();
        void InvalidateMeasure();
        void Measure(Size availableSize);
        void ReleaseMouseCapture();
        void RemoveHandler(RoutedEvent routedEvent, Delegate handler);
        GeneralTransform TransformToVisual(UIElement visual);
        void UpdateLayout();

        #endregion

        #region UserControl部分事件

        event EventHandler<GestureEventArgs> DoubleTap;
        event DragEventHandler DragEnter;
        event DragEventHandler DragLeave;
        event DragEventHandler DragOver;
        event DragEventHandler Drop;
        event RoutedEventHandler GotFocus;
        event EventHandler<GestureEventArgs> Hold;
        event DependencyPropertyChangedEventHandler IsEnabledChanged;
        event KeyEventHandler KeyDown;
        event KeyEventHandler KeyUp;
        event RoutedEventHandler LostFocus;
        event MouseEventHandler LostMouseCapture;
        event MouseEventHandler MouseEnter;
        event MouseEventHandler MouseLeave;
        event MouseButtonEventHandler MouseLeftButtonDown;
        event MouseButtonEventHandler MouseLeftButtonUp;
        event MouseEventHandler MouseMove;
        event MouseButtonEventHandler MouseRightButtonDown;
        event MouseButtonEventHandler MouseRightButtonUp;
        event MouseWheelEventHandler MouseWheel;
        event SizeChangedEventHandler SizeChanged;
        event EventHandler<GestureEventArgs> Tap;

        #endregion
    }
}

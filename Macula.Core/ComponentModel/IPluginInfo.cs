﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

using Gizeta.Macula.Plugin;

namespace Gizeta.Macula.ComponentModel
{
    public interface IPluginInfo
    {
        string Author { get; }
        string FullName { get; }
        Type InstanceType { get; }
        string Name { get; }
        PluginType Type { get; }
        string Website { get; }
        string Version { get; }
    }
}

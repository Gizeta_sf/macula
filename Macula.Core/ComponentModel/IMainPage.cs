﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

namespace Gizeta.Macula.ComponentModel
{
    public interface IMainPage
    {
        void ChangeConfigPageOriginalWidth(double value);
        void ChangeControlBarOriginalHeight(double value);
        void SetFullScreenStyle();
        void SetWideScreenStyle();
    }
}

﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;
using System.IO.IsolatedStorage;

namespace Gizeta.Macula.Utility
{
    /// <summary>
    /// 日志记录者
    /// <para>该类为静态类</para>
    /// </summary>
    public static class Logger
    {
        public static void Log(Exception error)
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    IsolatedStorageFileStream logFile = store.OpenFile("macula.log", FileMode.Append);
                    StreamWriter writer = new StreamWriter(logFile);
                    writer.WriteLine(string.Format("[{0}]:{1}", DateTime.Now, error.Message));
                    if (error.InnerException != null)
                    {
                        writer.WriteLine(error.InnerException.ToString());
                    }
                    writer.Close();
                }
            }
            catch { }
        }

        public static void Clean()
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists("macula.log"))
                    {
                        store.DeleteFile("macula.log");
                    }
                }
            }
            catch { }
        }
    }
}

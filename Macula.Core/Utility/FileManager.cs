﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.IO;
using System.IO.IsolatedStorage;

namespace Gizeta.Macula.Utility
{
    /// <summary>
    /// 文件管理者
    /// <para>该类不可继承</para>
    /// </summary>
    public sealed class FileManager
    {
        private static readonly FileManager instance = new FileManager();

        private FileManager()
        {
        }

        public static FileManager Instance
        {
            get { return instance; }
        }

        public IsolatedStorageFileStream OpenFile(string location)
        {
            try
            {
                var storage = IsolatedStorageFile.GetUserStoreForApplication();
                if (storage.FileExists(location))
                {
                    return storage.OpenFile(location, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    Logger.Log(new Exception("未找到文件:" + location));
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(new Exception("IsolatedStorageFile读取失败", ex));
                return null;
            }
        }

        public void SaveFile(string location, Stream content)
        {
            try
            {
                var storage = IsolatedStorageFile.GetUserStoreForApplication();
                var fileStream = storage.OpenFile(location, FileMode.Create, FileAccess.ReadWrite);
                if (content != null)
                {
                    content.CopyTo(fileStream);
                }
                fileStream.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(new Exception("IsolatedStorageFile读取失败", ex));
            }
        }
    }
}

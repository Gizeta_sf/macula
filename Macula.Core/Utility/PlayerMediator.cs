﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.Windows;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Controls;
using Gizeta.Macula.Plugin;
using Gizeta.Macula.Plugin.Parser;
using Gizeta.Macula.Plugin.Player;

namespace Gizeta.Macula.Utility
{
    public class PlayerMediator
    {
        private static readonly PlayerMediator mediator = new PlayerMediator();
        private PlayerConfig config = PlayerConfig.Instance;
        private IMainPage mainPage;
        private IPlayerComponent playerContainer;
        private INotificationComponent notificationPanel;
        private IControlComponent controlBar;
        private IConfigComponent configPage;

        private PlayerMediator()
        {
        }

        public static PlayerMediator Instance
        {
            get { return mediator; }
        }

        public IMainPage MainPage
        {
            get { return mainPage; }
            set { mainPage = value; }
        }

        public IPlayerComponent PlayerContainer
        {
            get { return playerContainer; }
            set { playerContainer = value; }
        }

        public INotificationComponent NotificationPanel
        {
            get { return notificationPanel; }
            set { notificationPanel = value; }
        }

        public IControlComponent ControlBar
        {
            get { return controlBar; }
            set { controlBar = value; }
        }

        public IConfigComponent ConfigPage
        {
            get { return configPage; }
            set { configPage = value; }
        }

        public void InitializeAllControls()
        {
            playerContainer.Initialize();
            notificationPanel.Initialize();
            controlBar.Initialize();
            configPage.Initialize();
        }

        public void PlayVideo(Dictionary<string, string> param)
        {
            InitializeAllControls();
            string valueString;
            if (param.TryGetValue("cover", out valueString))
            {
                playerContainer.SetCover(new Uri(valueString, valueString.Contains("://") ? UriKind.Absolute : UriKind.Relative));
            }
            if (param.TryGetValue("image", out valueString))
            {
                //playerContainer.Player.SetImage(new Uri(imageUrl, imageUrl.Contains("://") ? UriKind.Absolute : UriKind.Relative));
            }
            if (param.TryGetValue("vtype", out valueString))
            {
                IVideoParser videoParser = PluginInfoManager.Instance.GetPlugin(valueString, PluginType.VideoParser) as IVideoParser;
                if (videoParser != null)
                {
                    videoParser.Erred += videoParser_Erred;
                    videoParser.ParseCompleted += videoParser_ParseCompleted;
                    videoParser.Parse(param);
                }
                else
                {
                    notificationPanel.AddMessage("视频解析失败：未找到对应的解析工具！");
                }
            }
            else
            {
                notificationPanel.AddMessage("视频解析失败：未找到解析类型！");
            }
            bool isFirstRun = false;
            if (param.TryGetValue("firstRun", out valueString))
            {
                if (valueString == "true")
                {
                    isFirstRun = true;
                }
            }
            PlayerPluginAssemblyManager.Instance.InitializeAllPlugin(isFirstRun);
            PlayerPluginAssemblyManager.Instance.LoadPlugins(param, isFirstRun);
        }

        private void videoParser_ParseCompleted(object sender, ParseCompletedEventArgs e)
        {
            notificationPanel.AddMessage("视频解析成功。");

            playerContainer.Player.LoadVideo(e.Result);
        }

        private void videoParser_Erred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.Message);
            Logger.Log(e.ExceptionObject);
            e.Handled = true;
        }
    }
}

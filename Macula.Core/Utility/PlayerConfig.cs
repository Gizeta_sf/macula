﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Xml.Linq;

using Gizeta.Macula.Net;
using Gizeta.Macula.Plugin;
using Gizeta.Macula.Theme;

namespace Gizeta.Macula.Utility
{
    /// <summary>
    /// 播放器配置
    /// <para>实现INotifyPropertyChanged接口</para>
    /// </summary>
    public class PlayerConfig : INotifyPropertyChanged
    {
        public event EventHandler LoadCompleted;
        private static readonly PlayerConfig instance = new PlayerConfig();
        private IsolatedStorageSettings playerSettings;
        private PlayerMediator mediator = PlayerMediator.Instance;

        private PlayerConfig()
        {
            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                playerSettings = IsolatedStorageSettings.ApplicationSettings;
            }
        }

        public static PlayerConfig Instance
        {
            get { return instance; }
        }

        #region 公开属性

        private bool allowDefaultKeyboardControl = true;
        public bool AllowDefaultKeyboardControl
        {
            get { return allowDefaultKeyboardControl; }
            set
            {
                if (allowDefaultKeyboardControl != value)
                {
                    allowDefaultKeyboardControl = value;
                    OnPropertyChanged("AllowDefaultKeyboardControl");
                    SaveSettings("AllowDefaultKeyboardControl", value);
                }
            }
        }

        private bool allowDefaultMouseControl = true;
        public bool AllowDefaultMouseControl
        {
            get { return allowDefaultMouseControl; }
            set
            {
                if (allowDefaultMouseControl != value)
                {
                    allowDefaultMouseControl = value;
                    OnPropertyChanged("AllowDefaultMouseControl");
                    SaveSettings("AllowDefaultMouseControl", value);
                }
            }
        }

        private bool autoPlay = false;
        public bool AutoPlay
        {
            get { return autoPlay; }
            set
            {
                if (autoPlay != value)
                {
                    autoPlay = value;
                    OnPropertyChanged("AutoPlay");
                    SaveSettings("AutoPlay", value);
                }
            }
        }

        private bool isFullScreen = false;
        public bool IsFullScreen
        {
            get { return isFullScreen; }
            set
            {
                if (isFullScreen != value)
                {
                    isFullScreen = value;
                    Application.Current.Host.Content.IsFullScreen = value;
                    OnPropertyChanged("IsFullScreen");
                }
            }
        }

        private bool isLooped = false;
        public bool IsLooped
        {
            get { return isLooped; }
            set
            {
                if (isLooped != value)
                {
                    isLooped = value;
                    OnPropertyChanged("IsLooped");
                    SaveSettings("IsLooped", value);
                }
            }
        }

        private bool isMuted = false;
        public bool IsMuted
        {
            get { return isMuted; }
            set
            {
                if (isMuted != value)
                {
                    isMuted = value;
                    mediator.PlayerContainer.Player.IsMuted = value;
                    OnPropertyChanged("IsMuted");
                    SaveSettings("IsMuted", value);
                }
            }
        }

        private bool isWideScreen = false;
        public bool IsWideScreen
        {
            get { return isWideScreen; }
            set
            {
                if (isWideScreen != value)
                {
                    isWideScreen = value;
                    OnPropertyChanged("IsWideScreen");
                    mediator.MainPage.SetWideScreenStyle();
                    SaveSettings("IsWideScreen", value);
                }
            }
        }

        private double volume = 0.7;
        public double Volume
        {
            get { return volume; }
            set
            {
                if (value > 1)
                {
                    value = 1;
                }
                else if (value < 0)
                {
                    value = 0;
                }
                if (volume != value)
                {
                    volume=value;
                    mediator.PlayerContainer.Player.Volume = value;
                    OnPropertyChanged("Volume");
                    SaveSettings("Volume", value);
                }
            }
        }

        #endregion

        #region INotifyPropertyChanged接口实现

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region 公开函数

        public void Load(Uri url)
        {
            mediator = mediator ?? PlayerMediator.Instance; //避免mediator未实例化
            mediator.NotificationPanel.AddMessage("正在载入设置...");

            loadSettings();

            IDownloader downloader = DownloaderFactory.GetDownloader("StreamDownloader");
            downloader.Erred += downloader_Erred;
            downloader.DownloadCompleted += downloader_DownloadCompleted;
            downloader.Download(url);
        }

        public object ReadSettings(string key)
        {
            if (playerSettings.Contains(key))
            {
                return playerSettings[key];
            }
            return null;
        }

        public void SaveSettings(string key, object value)
        {
            playerSettings[key] = value;
        }

        public void SaveSettings(Dictionary<string, object> settings)
        {
            foreach (KeyValuePair<string, object> data in settings)
            {
                playerSettings[data.Key] = data.Value;
            }
        }

        #endregion

        private void loadSettings()
        {
            if (playerSettings.Contains("AllowDefaultKeyboardControl"))
            {
                AllowDefaultKeyboardControl = (bool)playerSettings["AllowDefaultKeyboardControl"];
            }
            if (playerSettings.Contains("AllowDefaultMouseControl"))
            {
                AllowDefaultMouseControl = (bool)playerSettings["AllowDefaultMouseControl"];
            }
            if (playerSettings.Contains("AutoPlay"))
            {
                AutoPlay = (bool)playerSettings["AutoPlay"];
            }
            if (playerSettings.Contains("IsLooped"))
            {
                IsLooped = (bool)playerSettings["IsLooped"];
            }
            if (playerSettings.Contains("IsMuted"))
            {
                IsMuted = (bool)playerSettings["IsMuted"];
            }
            if (playerSettings.Contains("IsWideScreen"))
            {
                IsWideScreen = (bool)playerSettings["IsWideScreen"];
            }
            if (playerSettings.Contains("Volume"))
            {
                Volume = (double)playerSettings["Volume"];
            }
        }

        private void loadFromXml(Stream source)
        {
            XDocument xmlDoc = XDocument.Load(source);
            XElement xml = xmlDoc.Element("Macula");
            if (xml == null)
            {
                return;
            }
            mediator.NotificationPanel.AddMessage("设置载入完成。");
            
            loadTheme(xml);
            loadPlugin(xml);
        }

        private void loadTheme(XElement xml)
        {
            if (xml.Element("Theme") != null)
            {
                foreach (XElement xe in xml.Element("Theme").Descendants("item"))
                {
                    if (xe.Attribute("name") != null)
                    {
                        string name = xe.Attribute("name").Value;
                        Uri url = new Uri(xe.Value, xe.Value.Contains("://") ? UriKind.Absolute : UriKind.Relative);
                        ThemeManager.Instance.AddTheme(name, url);
                    }
                    else
                    {
                        Logger.Log(new Exception("主题缺少name属性:" + xe.Value));
                    }
                }
            }
        }

        private void loadPlugin(XElement xml)
        {
            PluginLoader pluginLoader = new PluginLoader();
            pluginLoader.LoadCompleted += pluginLoader_LoadCompleted;
            pluginLoader.DownloadCountChanged += pluginLoader_DownloadCountChanged;
            pluginLoader.Load(xml);
        }

        #region 私有事件处理

        private void pluginLoader_DownloadCountChanged(object sender, DownloadCountChangedEventArgs e)
        {
            mediator.NotificationPanel.AddMessage(string.Format("正在载入插件...已完成{0}/{1}", e.CurrentDownloaded, e.TotalDownload));
        }

        private void pluginLoader_LoadCompleted(object sender, LoadCompletedEventArgs e)
        {
            PluginLoader pluginLoader = sender as PluginLoader;
            pluginLoader.LoadCompleted -= pluginLoader_LoadCompleted;
            pluginLoader.DownloadCountChanged -= pluginLoader_DownloadCountChanged;

            mediator.NotificationPanel.AddMessage(string.Format("插件载入完成。{0}", e.WrongLoaded == 0 ? "" : string.Format("{0}个插件未成功载入。", e.WrongLoaded)));

            mediator.ConfigPage.BindPluginList();

            if (this.LoadCompleted != null)
            {
                this.LoadCompleted(this, null);
            }
        }

        private void downloader_DownloadCompleted(object sender, DownloadCompletedEventArgs e)
        {
            IDownloader downloader = sender as IDownloader;
            downloader.Erred -= downloader_Erred;
            downloader.DownloadCompleted -= downloader_DownloadCompleted;
            
            loadFromXml((Stream)e.Result);
        }

        private void downloader_Erred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            IDownloader downloader = sender as IDownloader;
            downloader.Erred -= downloader_Erred;
            downloader.DownloadCompleted -= downloader_DownloadCompleted;

            mediator.NotificationPanel.AddMessage("设置载入失败。");

            Logger.Log(e.ExceptionObject);
            e.Handled = true;
        }

        #endregion
    }
}

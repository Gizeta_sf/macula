﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Video;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// 播放器接口
    /// <para>继承Gizeta.Macula.ComponentModel.IComponent接口</para>
    /// </summary>
    public interface IPlayer : IComponent
    {
        int AudioStreamCount { get; }
        int? AudioStreamIndex { get; set; }
        bool AutoPlay { get; set; }
        double Balance { get; set; }
        double BufferingProgress { get; }
        TimeSpan BufferingTime { get; set; }
        bool CanPause { get; }
        bool CanSeek { get; }
        MediaElementState CurrentState { get; }
        double DownloadProgress { get; }
        double DownloadProgressOffset { get; }
        double DroppedFramesPerSecond { get; }
        bool IsMuted { get; set; }
        TimelineMarkerCollection Markers { get; }
        Duration NaturalDuration { get; }
        int NaturalVideoHeight { get; }
        int NaturalVideoWidth { get; }
        double PlaybackRate { get; set; }
        TimeSpan Position { get; set; }
        double RenderedFramesPerSecond { get; }
        Uri Source { get; set; }
        Stretch Stretch { get; set; }
        double Volume { get; set; }

        IPlayer LoadVideo(VideoInfo videoInfo);
        IPlayer Pause();
        IPlayer Play();
        IPlayer SetSource(MediaStreamSource source);
        IPlayer SetSource(Stream source);
        IPlayer Stop();

        event RoutedEventHandler BufferingProgressChanged;
        event RoutedEventHandler CurrentStateChanged;
        event RoutedEventHandler DownloadProgressChanged;
        event TimelineMarkerRoutedEventHandler MarkerReached;
        event RoutedEventHandler MediaEnded;
        event EventHandler<ExceptionRoutedEventArgs> MediaFailed;
        event RoutedEventHandler MediaOpened;
        event RateChangedRoutedEventHandler RateChanged;
    }
}

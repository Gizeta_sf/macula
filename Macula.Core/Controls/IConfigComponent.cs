﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula设置页接口
    /// <para>继承Gizeta.Macula.ComponentModel.IComponent接口</para>
    /// </summary>
    public interface IConfigComponent : IComponent
    {
        void BindPluginList();
    }
}

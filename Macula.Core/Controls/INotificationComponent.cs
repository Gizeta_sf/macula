﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// 消息中心控件接口
    /// <para>继承Gizeta.Macula.ComponentModel.IComponent接口</para>
    /// </summary>
    public interface INotificationComponent : IComponent
    {
        INotificationComponent AddMessage(string message);

        INotificationComponent HideMessage();
    }
}

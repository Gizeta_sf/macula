﻿/*
 * Copyright (c) 2012-2013 Gizeta
 * This content is released under the MIT License.
 * See the file LICENSE.txt for copying permission.
 */

using System;

using Gizeta.Macula.ComponentModel;

namespace Gizeta.Macula.Controls
{
    /// <summary>
    /// Macula播放器组件接口
    /// <para>继承Gizeta.Macula.ComponentModel.IComponent接口</para>
    /// </summary>
    public interface IPlayerComponent : IComponent
    {
        IPlayer Player { get; }

        IPlayerComponent SetCover(Uri imageUrl);
    }
}
